# vue-api

> This API is specifically designed for learning Vue.js as part of the DWWM (Web and Mobile Web Developer) certification program in 2023-2024.
It will be primarily used for testing XHR/Fetch requests during the development of a Vue.js application.
> You can have a look to the [documentation](https://dwwm.gregdesplaces.com/docs).

## About

This project uses [Feathers](http://feathersjs.com). An open source framework for building APIs and real-time applications.

## Getting Started

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/vue-api
    npm install
    ```

3. Start your app

    ```
    npm run dev # Run the dev server
    npm run compile # Compile TypeScript source
    npm run migrate # Run migrations to set up the database
    npm start
    ```

## Testing

Run `npm test` and all your tests in the `test/` directory will be run.

## Scaffolding

This app comes with a powerful command line interface for Feathers. Here are a few things it can do:

```
$ npx feathers help                           # Show all commands
$ npx feathers generate service               # Generate a new Service
```

## Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).
